# Team Roadmap

Roadmap of the Team on Deep Learning and Hardware Acceleration

## 1. Deep Learning Framework

Generally speaking, these deep learning frameworks are maintained by Debian Deep Learning Team.

### 1.1. TensorFlow

One of the top-2 deep learning frameworks.

1. bazel: status?
2. tensorflow-cpu: status?
3. tensorflow-gpu: status?

Updates:
* ...

### 1.2. PyTorch

One of the top-2 deep learning frameworks.

* pytorch (cpu version) status: Already in the archive: https://tracker.debian.org/pkg/pytorch
* pytorch-cuda: blocked(postponed) due to absense of nvidia-cudnn

Plan:
* Stick to pytorch/v1.8.1 for a while. The 1.8.X is an LTS release.

Updates:
* May 21, 2021: `lumin` is recently preparing pytorch/v1.8.1 upload.

### 1.3. Caffe

Caffe was ever a prevalent framework. Due to its architecture design and its limitation in flexibility, it was no longer actively developed. However, this is still a very valuable educational code base to demonstrate how a deep learning framework works. To reduce maintainence burden, I removed the cuda version of caffe from the archive. -- `lumin`

* caffe-cpu: in the archive: https://tracker.debian.org/pkg/caffe
* caffe-cuda: removed from the archive.

### 1.4. MxNet

???

### 1.5. Torch7 (Lua-based)

Once prevalent, but already deprecated. Removed from the archive.

### 1.5. Others

Not yet considered.

## 2. Hardware Acceleration

Reference: Poll: how far should we go? https://lists.debian.org/debian-ai/2021/05/msg00019.html

The following hardware acceleration solutions involve different teams within Debian. Collaboration with other teams are required.

### 2.1. Nvidia/CUDA

Maintained by nvidia-pkg team.

* nvidia-cuda-toolkit status: in archive (non-free section).

Remaining works:
1. package `nvidia-cudnn`. This blocks the cuda version of deep learning frameworks.

### 2.2. AMD/ROCm

Maintained by `Debian ROCm Team <debian-ai@l.d.o>`. But we may need to colaborate with OpenCL and LLVM teams.

* ROCM status: only a few packages are in the archive.

### 2.3. Intel/SYCL (oneAPI)

We may need to collaborate with OpenCL (for SYCL) and LLVM teams for this.

* SYCL status: no any plan yet.

## 3. Debian Infrastructure

We need the corresponding hardwares to facilitate the development.

### 3.1. Porterbox

### 3.2. CI

### 3.3. Buildd
